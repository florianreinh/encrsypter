mod encrypt_decrypt_steps;
mod world;

fn main() {
    // Do any setup you need to do before running the Cucumber runner.
    // e.g. setup_some_db_thing()?;
    let runner = cucumber::Cucumber::<world::EncrsypterTestWorld>::new()
        .features(&["./tests/features/"])
        .steps(crate::encrypt_decrypt_steps::steps())
        .debug(true);

    // You may choose any executor you like (Tokio, async-std, etc)
    // You may even have an async main, it doesn't matter. The point is that
    // Cucumber is composable. :)
    futures::executor::block_on(runner.run());
}
