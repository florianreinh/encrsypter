use cucumber::{t, Steps};
use std::borrow::Cow;
use std::fs;
use std::path::Path;

// Rust handles .rs files under /tests differently from files under /src,
// as explained here
// https://doc.rust-lang.org/book/ch11-03-test-organization.html#submodules-in-integration-tests
// we have to resort to the old module structuring: <module_name_as_folder>/mod.rs as recommended
// in the link above.

pub fn steps() -> Steps<crate::world::EncrsypterTestWorld> {
    let mut builder: Steps<crate::world::EncrsypterTestWorld> = Steps::new();

    builder
        .given_regex_async(
            "^I have an encryptor initialized with input \"([\\w,\\s,!]+)\"$",
            t!(|mut world, ctx| {
                world.encryptor.input = Cow::Owned(ctx.matches[1].to_owned());
                world
            }),
        )
        .then_regex_async(
            "^I should see \"([\\w,\\s,!]+)\" in the test encryptor's input field",
            t!(|world, ctx| {
                assert_eq!(ctx.matches[1], world.encryptor.input);
                world
            }),
        )
        .when_async(
            "I encrypt the encryptor's input",
            t!(|world, _ctx| {
                world.encryptor.write_encrypted();
                world
            }),
        )
        .then_async(
            "testfile.txt exists",
            t!(|_world, _ctx| {
                let testfile_path = Path::new("./testfile.txt");
                assert_eq!(testfile_path.exists(), true);
                _world
            }),
        )
        .then_async(
            "testfile.txt is not empty",
            t!(|mut world, _ctx| {
                let enc_message = fs::read("./testfile.txt").expect("Could not read test file.");
                world.encrypted_base64 = base64::encode(&enc_message);

                assert_eq!(world.encrypted_base64.len() > (0 as usize), true);
                world
            }),
        )
        .when_async(
            "I decrypt testfile.txt",
            t!(|mut world, _ctx| {
                world.decrypt_result = world.decryptor.read_decrypted();
                world
            }),
        )
        .then_regex_async(
            "^the decrypted result should be \"([\\w,\\s,!]+)\"$",
            t!(|mut world, ctx| {
                assert_eq!(ctx.matches[1], world.decrypt_result);
                world
            }),
        )
        .when_async(
            "I test print to STDOUT",
            t!(|mut world, _ctx| {
                println!("TEST STDOUT!");
                world
            }),
        )
        .when_async(
            "I test print to STDERR",
            t!(|mut world, _ctx| {
                eprintln!("TEST STDERR!");
                world
            }),
        );

    builder
}
