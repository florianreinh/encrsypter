use async_trait::async_trait;
use encrsypter_lib::{decryptor, encryptor};
use std::borrow::Cow;
use std::convert::Infallible;

// Rust handles .rs files under /tests differently from files under /src,
// as explained here
// https://doc.rust-lang.org/book/ch11-03-test-organization.html#submodules-in-integration-tests
// we have to resort to the old module structuring: <module_name_as_folder>/mod.rs as recommended
// in the link above.

pub struct EncrsypterTestWorld {
    pub encryptor: encryptor::Encryptor<'static>,
    pub decryptor: decryptor::Decryptor<'static>,
    pub encrypted_base64: String,
    pub decrypt_result: String,
}

#[async_trait(?Send)]
impl cucumber::World for EncrsypterTestWorld {
    type Error = Infallible;

    // Much more straightforward than the Default Trait before. :)
    async fn new() -> Result<Self, Infallible> {
        let key = &[1; 32];
        let nonce = &[3; 12];

        Ok(Self {
            encryptor: encryptor::Encryptor {
                input: Cow::Borrowed(""),
                key,
                nonce,
            },
            decryptor: decryptor::Decryptor {
                file_path: "./testfile.txt",
                key,
                nonce,
            },
            encrypted_base64: "".to_string(),
            decrypt_result: "".to_string(),
        })
    }
}
